#pragma once
#include"fstream"
#include"string"
#include"iostream"
#include"Dxlib.h"
using namespace std;

/*
typedef struct tag_MapData
{
	int MapNum;  //マップ番号
	int MapEffect; //マップ効果
	int x; // マップX座標
	int y; //マップY座標
	
}MapData;
*/



class Map{
private:
	int Gr_mapback; //マップ背景 
	int Gr_road; //道画像
	int map[7][9];
	ifstream fileopen;  //ファイルを読むポインタ 
	string Line;
	//MapData MData[41];	// マップデータ
	int Gr_chip[3];		// マス画像
	int Normal_chip; //効果無マス
	int Gr_graphic[5];  // お城とアジト画像

public:
	Map();
	~Map();

	// マップエフェクト
	void MapEffect(int id);	// マップ効果

	void Draw();	// 描画

	int get_MapEffects(int i, int j);
};