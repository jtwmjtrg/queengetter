#pragma once
#include "DxLib.h"

class Result {
private:
	int Result_Graph; //画像
	int Result_BGM; //ファンファーレ的な

	bool nextMode; //このフラグでフローが進行する

	//ゲームパッド用あとで島田君が治す
	bool Return; //エンターキー対応

	//演出用
	bool ResultBGM_Flag; //BGMを一回しか流さないための
	int Animation; //アニメーションのカウント
	int y;
	int speed; //加速調整
	int brake; //ブレーキ

public:
	Result(int Winner/*勝者のid*/);
	~Result(); //凸

	void update_Result(XINPUT_STATE input); //主に入力関係
	void draw_Result(); //主に描写関係

	void set_nextMode(bool nextMode); //セッター
	bool get_nexctMode(); //ゲッター
};