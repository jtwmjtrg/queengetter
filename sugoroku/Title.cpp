#include "Title.h"

Title::Title() {
	Title_Graph = LoadGraph("素材\\title.png");
	Title_BGM = LoadSoundMem("素材\\op.mp3");
	Title_Sound_SE = LoadSoundMem("素材\\title_sound_SE.wav");
	carsol = LoadGraph("素材\\Princess.png");

	y = 700;
	modeselect = true;
	end_Flag = false;

	nextMode = FALSE;
	Return = TRUE;
	TitleBGMF = false;
}
Title::~Title() {
	DeleteSoundMem(Title_BGM);

}

void Title::updateTitle(XINPUT_STATE input) {
	if (modeselect == true){
		if (GetKey(KEY_INPUT_DOWN) >= 1 || input.Buttons[1] >= 1) {
			y += 180;
			modeselect = false;

		}
	}
	if (modeselect == false){
		if (GetKey(KEY_INPUT_UP) >= 1 || input.Buttons[0] >= 1) {
			y -= 180;
			modeselect = true;

		}
	}
	//ゲーム画面へ
	if (Return != TRUE) { //此処も島田君な
		if (GetKey(KEY_INPUT_RETURN) >= 1 || input.Buttons[12] >= 1) {
			if (modeselect == true) {
				PlaySoundMem(Title_Sound_SE, DX_PLAYTYPE_BACK);
				nextMode = TRUE; //ゲーム画面へ
			}
			else {
				end_Flag = true;
			}

			Return = TRUE;
		}
	}
	else {
		if (CheckHitKey(KEY_INPUT_RETURN) == FALSE &&input.Buttons[12] == false) {
			Return = FALSE;
		}
	}
}

void Title::drawTitle() {
	DrawGraph(0, 0, Title_Graph, TRUE);
	if (TitleBGMF == false){
		PlaySoundMem(Title_BGM, DX_PLAYTYPE_BACK, true); //drawとかいいながら音楽も流す人間の屑
		TitleBGMF = true;
	}

	DrawGraph(700, y, carsol, true);
	DrawGraph(1230, y, carsol, true);
}

void Title::set_nextMode(bool nextMode) {
	this->nextMode = nextMode;
}
bool Title::get_nexctMode() {
	return nextMode;
}
bool Title::get_endFlag() {
	return end_Flag;
}