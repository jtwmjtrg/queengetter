#include "Gamemain.h"

typedef enum tag_Gamemode { LEFT = 1, RIGHT, UP, DOWN };

Gamemain::Gamemain(){
	backBGM = LoadSoundMem("素材\\Let`s party.mp3");
	BGMF = false;
	diceroll_sound = LoadSoundMem("素材\\diceroll_sound.wav");
	move_sound = LoadSoundMem("素材\\move_sound.wav");
	move_cancel_sound = LoadSoundMem("素材\\move_cancel_sound.wav");
	turn_end_sound = LoadSoundMem("素材\\turn_end_sound.wav");

	LoadDivGraph("素材\\Dice.png", 6, 6, 1, 128, 128, Gr_dice);
	LoadDivGraph("素材\\number.png", 10, 10, 1, 192, 192, Gr_number);
	this->Gr_TurnP[0] = LoadGraph("素材\\turn1P.png");
	this->Gr_TurnP[1] = LoadGraph("素材\\turn2P.png");
	this->Gr_TurnP[2] = LoadGraph("素材\\turn3P.png");
	this->Gr_TurnP[3] = LoadGraph("素材\\turn4P.png");

	text_Graph = LoadGraph("素材\\textbox.png");
	Color = GetColor(255, 0, 0);

	turnplayer = 0;
	turnplayer_x = 0;
	turnplayer_y = 0;

	flow = START;

	for (int i = 0; i < 10; i++){
		movedirection[i] = 0; //全ての方向の移動数を0にする
	}

	dicesum = 0;
	movepoint = 0;
	Bmovepoint = 0;
	wait = 0;
	diceroll_sound_Flag = false;

	//姫関係
	princess_Graph = LoadGraph("素材\\Princess.png");
	princess_x = 4;
	princess_y = 3;
	Hiace = false;

	//battle関係
	battle_Graph = LoadGraph("素材\\battle_Graph.png");
	attackpower = 0;
	defensepower = 0;
	battlecount = 0;
	this->BattleDelay = 0;
	this->BattleStep = 0;
	/*
	for (int i = 0; i < 5;i++){
		battle_Flag[i] = false;
		attacker[i] = 4; //初期値4(ドのプレイヤーも差さない)
		defender[i] = 4;
	}
	*/

	dicestop = false;
	attack_dice = 0;
	defence_dice = 0;
	rensen = 0;
	battle_x = 0;
	battle_y = 224;

	Return = TRUE;
	Left = TRUE;
	Right = TRUE;
	Up = TRUE;
	Down = TRUE;
	Q = true;

	nextMode = false;
	winner = 0;

	for (int i = 0;i < 4;i++) {
		player[i] = new Player(i);
	}
	map = new Map();

	backBGM2 = LoadSoundMem("素材\\main_BGM.wav"); //BGM２コ目
	BGMF2 = false; //フラグも２コメ
}

Gamemain::~Gamemain(){
	DeleteSoundMem(backBGM);
	DeleteSoundMem(backBGM2); //ver1.1追加
}

int Gamemain::diceroll(int dicenum, int dicemax, int correction) { //引数(ダイスの数,ダイスの最大値,補正値)

	if (diceroll_sound_Flag == false) {

		PlaySoundMem(diceroll_sound, DX_PLAYTYPE_BACK); //ダイスロール音
		diceroll_sound_Flag = true;
	}

	dicesum = 0; //初期化

	for (int i = 0;i < dicenum;i++) {
		dicesum += GetRand(dicemax - correction - 1) + 1;
	}

	return dicesum;
}

void Gamemain::draw_Text(char *name, char *s) {
	//DrawGraph(0, 1080 - 416, text_Graph, TRUE);
	DrawFormatString(640, 540, Color, name);
	DrawFormatString(640, 604, Color, s);
}
void Gamemain::draw_Text(char *name, int x, char *s, int y) {
	//DrawGraph(0 + x, 1080 - 416 - y, text_Graph, TRUE);
	DrawFormatString(640, 540, Color, name, x);
	DrawFormatString(640, 604, Color, s, y);
}

Draw_Text::Draw_Text(char *name, char *s, int wait) {
	int WHITE = GetColor(255, 255, 255);
	this->name = name;
	this->s = s;
	this->Type = true;
	this->wait = wait;
}
Draw_Text::Draw_Text(char *name, int x, char *s, int y, int wait) {
	int WHITE = GetColor(255, 255, 255);
	this->name = name;
	this->s = s;
	this->x = x;
	this->y = y;
	this->Type = false;
	this->wait = wait;
}

Draw_Text::~Draw_Text() {
}

bool Draw_Text::UpDate() {
	wait--;
	return wait <= 0;
}

void Draw_Text::Draw() {
	if (this->Type) {
		DrawFormatString(640, 540, WHITE, name);
		DrawFormatString(640, 604, WHITE, s);
	}
	else {
		DrawFormatString(640, 540, WHITE, name, x);
		DrawFormatString(640, 604, WHITE, s, y);
	}
}


//戦闘処理(攻撃側、防御側)負けたプレイヤー番号を返す
int Gamemain::battle(int attack, int defense) {
	//attacker[battlecount] = 4;
	//defender[battlecount] = 4; //初期値

	attack_dice = diceroll(1, 6, 0);
	defence_dice = diceroll(1, 6, 0);

	if (attack_dice < defence_dice) {	// アタックの負け
		//wait = 0;

		//attack_dice = 0;
		//defence_dice = 0;
		/*
		for (int i = 0; i < 5; i++){
			battle_Flag[i] = false;
			attacker[i] = 4; //初期値4(ドのプレイヤーも差さない)
			defender[i] = 4;
		}
		*/
		//this->player[attack]->death();
		if (player[attack]->Queengetter()) {	// アタッカー側が姫をもっていたら
			this->BattleStep = 3;	// 全戦闘削除画面に
		}
		else {
			this->BattleStep = 2;	// 戦闘削除画面に
		}
		player[defense]->Queensetter(true);	// ディフェンス側に姫を移動
		return attack;
	}
	else if (attack_dice > defence_dice) {	// ディフェンスの負け
		//this->player[defense]->death();
		player[attack]->Queensetter(true);	// アタッカー側に姫を移動
		this->BattleStep = 2;	// 戦闘削除画面に
		return defense;
	}
	else {//引き分け処理
		return battle(attack, defense);
		//return defense;
	}
}

void Gamemain::update(XINPUT_STATE input){
	map->Draw(); //マップデータは此処で描写
	//Key_Update();
	//draw_Text("", 0, "%d", this->attacker.size());

	if (this->BattleDelay > 0)
	{
		this->BattleDelay--;
		if (BattleDelay <= 0 && (input.Buttons[12] == false && CheckHitKey(KEY_INPUT_RETURN) == false)) //ver1.1追加
		{
			BattleDelay++;
		}
		return;
	}


	switch (flow) {
	case START: //ターン開始

		draw_Text("GM", "ターン開始です!");

		//エンター押したら
		if (Return == FALSE) {
			if (CheckHitKey(KEY_INPUT_RETURN) >= 1 || input.Buttons[12] >= 1) {

				turnplayer_x = player[turnplayer]->get_x();
				turnplayer_y = player[turnplayer]->get_y();
				player[turnplayer]->set_Bx(turnplayer_x);
				player[turnplayer]->set_By(turnplayer_y); //座標保存

				battlecount = 0;

				PlaySoundMem(diceroll_sound, DX_PLAYTYPE_BACK); //ダイスロール音

				Return = true;
				flow = DICEROLL;
			}
		}
		else {
			if (CheckHitKey(KEY_INPUT_RETURN) == 0 && input.Buttons[12] == 0) {
				Return = FALSE;
			}
		}

		break;
	case DICEROLL: //ダイスロール

		if (wait == 128) {

			wait = 0;

			diceroll_sound_Flag = false; //ダイスロール音復活

			flow = MOVE;
		}
		else if (wait < 64) {
			movepoint = diceroll(player[turnplayer]->dicenumgetter(), 3, player[turnplayer]->dicecorrectiongetter()); //ダイスロール
			Bmovepoint = movepoint;

			wait++;
		}
		else {
			movepoint = Bmovepoint;

			wait++;
		}

		break;
	case MOVE: //（進路選択)

		move(input); //操作

		break;
	case BATTLE: //（戦闘）

		if (!this->attacker.empty()) {	// 戦闘がまだ残っていれば
			switch (this->BattleStep) {
			case 0:	// ダイスロール
				this->BattleDelay = 64;	// 64フレームストップ
				dicestop = false;	// ダイスを動かす
				this->BattleStep = 1;	// 戦闘画面に
				break;
			case 1:	// 戦闘
				player[battle(attacker[0], defender[0])]->death();	// 負けた方が死亡
				this->BattleDelay = 64;	// 64フレームストップ
				dicestop = true;	// ダイスを止める
				break;
			case 2:
				attacker.pop_front();	// 戦闘済みのプレイヤーを削除
				defender.pop_front();
				dicestop = false;	// ダイスを動かす
				this->BattleStep = 0;	// ダイスロールor終了画面に
				break;
			case 3:
				// 全戦闘削除
				attacker.clear();
				defender.clear();
				attacker.shrink_to_fit();
				defender.shrink_to_fit();
				dicestop = false;	// ダイスを動かす
				this->BattleStep = 0;	// ダイスロールor終了画面に
				break;
			}
		}
		else {	// 戦闘終了
			turnplayer_x = player[turnplayer]->get_x();	// 座標保存
			turnplayer_y = player[turnplayer]->get_y();

			flow = EFFECTS;	// マス効果シーンへ
		}

		/*
		if (battle_Flag[battlecount] == true) { //battleフラグ

			if (wait == 64) {
				player[attacker[battlecount]]->Queensetter(true);
				player[defender[battlecount]]->Queensetter(true); //力技誰か助けてPLayer.cppのdeath()で姫を破棄できるのを利用してます
				battle(attacker[battlecount], defender[battlecount]);

				dicestop = true;

				wait++;
			}
			else if (wait == 128) {
				wait = 0;

				attack_dice = 0;
				defence_dice = 0;
				dicestop = false;

				battle_Flag[battlecount] = false;
				battlecount++;
			}
			else {
				wait++;
			}

		}
		else if (battlecount == 4){		// 戦闘なし

			turnplayer_x = player[turnplayer]->get_x();
			turnplayer_y = player[turnplayer]->get_y();

			flow = EFFECTS;
		}
		else {
			battlecount++;
		}
		*/

		break;
	case EFFECTS: //（姫＆マス効果）


		//プレイヤー側の調整
		player[turnplayer]->dicenumsetter(1);
		player[turnplayer]->dicecorrectionsetter(0);

		MapEffect();

		break;
	case END: //ターンエンド

		turnplayer++; //次のプレイヤーへ
		if (turnplayer > 3)
		{
			turnplayer = 0;
		}

		PlaySoundMem(turn_end_sound, DX_PLAYTYPE_BACK);

		flow = START;

		break;
	default:
		/*例外な此処*/
		break;
	}

}

void Gamemain::draw(){
	if (BGMF == false){
		PlaySoundMem(backBGM, DX_PLAYTYPE_LOOP, true); //drawとかいいながら音楽も流す人間の屑
		BGMF = true;
	}
	if (Hiace == true && BGMF2 == false) //ver1.1追加
	{
		StopSoundMem(backBGM);
		PlaySoundMem(backBGM2, DX_PLAYTYPE_LOOP);
		BGMF2 = true;
	}

	// 誰のターンか表示
	DrawGraph(864, 50, Gr_TurnP[turnplayer], true);

	for (int i = 0;i < 4;i++) {
		player[i]->draw(180 + (195 * player[i]->get_x()), 92 + (150 * player[i]->get_y()));

		if (flow == MOVE && i == turnplayer) {
			DrawFormatString(1216 - 192, 316, Color, "あと");
			DrawRotaGraph(1216, 348, 0.5, 0, Gr_number[movepoint], true, 0); //残り移動数
		}
	}

	if (player[0]->Queengetter() == false && player[1]->Queengetter() == false && player[2]->Queengetter() == false && player[3]->Queengetter() == false) { //＊しろのなかにいる＊
		DrawRotaGraph(180 + (195 * princess_x), 92 + (150 * princess_y) - 128, 1, 0, princess_Graph, true, 0);
	}

	if (flow == DICEROLL) {
		DrawGraph(960-64, 540-64, Gr_dice[movepoint - 1], true);
	}

	if (flow == BATTLE) {

		//if (battle_Flag[battlecount] == true) {
		if(!this->attacker.empty() || this->dicestop){
			DrawGraph(0, battle_y, battle_Graph, true);

			//DrawFormatString(256, battle_y, Color, "%dP", turnplayer + 1);
			//DrawFormatString(1320, battle_y, Color, "%dP", battlecount + 1);
			DrawFormatString(256, battle_y, Color, "%dP", attacker[0] + 1);
			DrawFormatString(1320, battle_y, Color, "%dP", defender[0] + 1);

			for (int i = 0;i < 2;i++) {

				if (!this->dicestop) {
					// ダイスロール中
					draw_Text("", "  battleスタート!");
					DrawGraph(256 + (1064 * i) - 32, battle_y + 256, Gr_dice[diceroll(1, 6, 0) - 1], true);
				}
				else {	// ダイスロール終了後
					if (i == 0) {
						DrawGraph(256 + (1064 * i) - 32, battle_y + 256, Gr_dice[attack_dice - 1], true);
					}
					else {
						DrawGraph(256 + (1064 * i) - 32, battle_y + 256, Gr_dice[defence_dice - 1], true);
					}

					if (attack_dice > defence_dice) {
						draw_Text("", 0, "  %dPの勝利！", turnplayer + 1);
					}
					else {
						draw_Text("", "  防御側の勝利！");
					}
				}
			}
		}

	}


	//////////////////////////////////////////////////
	//draw_Text("", 0, "%d", player[turnplayer]->dicenumgetter());
}

void Gamemain::move(XINPUT_STATE input){
	if (movepoint != 0) {
		//GMメッセージ
		/*DrawFormatString(100, 0, Color, "あと%d動けます", movepoint);
		DrawFormatString(500, 0, GetColor(255, 255, 255), "%d%d%d\n%d%d%d\n%d%d%d\n%d%d%d", player[0]->get_x(), player[0]->get_y(), player[0]->Queengetter(),
			player[1]->get_x(), player[1]->get_y(), player[1]->Queengetter(), player[2]->get_x(), player[2]->get_y(), player[2]->Queengetter(), player[3]->get_x(),
			player[3]->get_y(), player[3]->Queengetter());
			*/

		if (player[turnplayer]->get_inCastle() != 4) {	// 城にいる

			switch (player[turnplayer]->get_inCastle()) {	// 城に入った方向
			case 0:
				turnplayer_y -= 2;
				break;
			case 1:
				turnplayer_y += 2;
				break;
			case 2:
				turnplayer_x -= 2;
				break;
			case 3:
				turnplayer_x += 2;
				break;
			default:
				break;
			}

			movepoint--;

			player[turnplayer]->set_x(turnplayer_x);
			player[turnplayer]->set_y(turnplayer_y);
			player[turnplayer]->set_Bx(turnplayer_x);
			player[turnplayer]->set_By(turnplayer_y); //座標保存

			Bmovepoint = movepoint;

			player[turnplayer]->set_inCastle(4); //4=初期値

			if (movepoint == 0) { //ループ防止(城で出目1→ワープ踏む→城へ→…)
				flow = END; //此処どうにかしなきゃダメ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			}
		}
		else {
			if (Left == FALSE) {
				if (CheckHitKey(KEY_INPUT_LEFT) >= 1 || input.Buttons[2] >= 1) {

					if (player[turnplayer]->get_direction() == 3) { //来た道を戻ったら
						move_cancel();
					}
					else if (turnplayer_x == 3 && turnplayer_y == 1) { //例外
						turnplayer_x--;
						turnplayer_y++;
						player[turnplayer]->set_x(turnplayer_x);
						player[turnplayer]->set_y(turnplayer_y);
						
						player[turnplayer]->set_direction(1);

						movepoint--;
					}
					else if (turnplayer_x == 3 && turnplayer_y == 5) { //例外
						turnplayer_x--;
						turnplayer_y--;
						player[turnplayer]->set_x(turnplayer_x);
						player[turnplayer]->set_y(turnplayer_y);

						player[turnplayer]->set_direction(0);

						movepoint--;
					}
					else if (map->get_MapEffects(turnplayer_y, turnplayer_x - 1) != 0 && turnplayer_x != 0) { //左が押され続けていない、隣が0(無)でない、マップ左端にいない{

						player[turnplayer]->set_x(--turnplayer_x); //左へ
						turnplayer_x = player[turnplayer]->get_x();

						player[turnplayer]->set_direction(2);

						movepoint--;

					}

					PlaySoundMem(move_sound, DX_PLAYTYPE_BACK);

					Left = true;
				}
			}
			else {
				if (CheckHitKey(KEY_INPUT_LEFT) == 0 && input.Buttons[2] == 0) {
					Left = FALSE;
				}
			}

			if (Right == FALSE) {
				if (CheckHitKey(KEY_INPUT_RIGHT) >= 1 || input.Buttons[3] >= 1) {

					if (player[turnplayer]->get_direction() == 2) { //来た道を戻ったら
						move_cancel();
					}
					else if (turnplayer_x == 5 && turnplayer_y == 1) { //例外
						turnplayer_x++;
						turnplayer_y++;
						player[turnplayer]->set_x(turnplayer_x);
						player[turnplayer]->set_y(turnplayer_y);

						player[turnplayer]->set_direction(1);

						movepoint--;
					}
					else if (turnplayer_x == 5 && turnplayer_y == 5) { //例外
						turnplayer_x++;
						turnplayer_y--;
						player[turnplayer]->set_x(turnplayer_x);
						player[turnplayer]->set_y(turnplayer_y);

						player[turnplayer]->set_direction(0);

						movepoint--;
					}
					else if (map->get_MapEffects(turnplayer_y, turnplayer_x + 1) != 0 && turnplayer_x != 8) { //左が押され続けていない、隣が0(無)でない、マップ左端にいない{{
						player[turnplayer]->set_x(++turnplayer_x); //右へ
						turnplayer_x = player[turnplayer]->get_x();

						player[turnplayer]->set_direction(3);

						movepoint--;
					}

					PlaySoundMem(move_sound, DX_PLAYTYPE_BACK);

					Right = true;
				}
			}
			else {
				if (CheckHitKey(KEY_INPUT_RIGHT) == 0 && input.Buttons[3] == 0) {
					Right = FALSE;
				}
			}

			if (Up == FALSE) {
				if (CheckHitKey(KEY_INPUT_UP) >= 1 || input.Buttons[0] >= 1) {

					if (player[turnplayer]->get_direction() == 1) { //来た道を戻ったら
						move_cancel();
					}
					else if (turnplayer_x == 2 && turnplayer_y == 2) { //例外
						turnplayer_x++;
						turnplayer_y--;
						player[turnplayer]->set_x(turnplayer_x);
						player[turnplayer]->set_y(turnplayer_y);

						player[turnplayer]->set_direction(3);

						movepoint--;
					}
					else if (turnplayer_x == 6 && turnplayer_y == 2) { //例外
						turnplayer_x--;
						turnplayer_y--;
						player[turnplayer]->set_x(turnplayer_x);
						player[turnplayer]->set_y(turnplayer_y);

						player[turnplayer]->set_direction(2);

						movepoint--;
					}
					else if (map->get_MapEffects(turnplayer_y - 1, turnplayer_x) != 0 && turnplayer_y != 0) {
						player[turnplayer]->set_y(--turnplayer_y); //上へ
						turnplayer_y = player[turnplayer]->get_y();

						player[turnplayer]->set_direction(0);

						movepoint--;
					}

					PlaySoundMem(move_sound, DX_PLAYTYPE_BACK);

					Up = true;
				}
			}
			else {
				if (CheckHitKey(KEY_INPUT_UP) == 0 && input.Buttons[0] == 0) {
					Up = FALSE;
				}
			}

			if (Down == FALSE) {
				if (CheckHitKey(KEY_INPUT_DOWN) >= 1 || input.Buttons[1] >= 1) {

					if (player[turnplayer]->get_direction() == 0) { //来た道を戻ったら
						move_cancel();
					}
					else if (turnplayer_x == 2 && turnplayer_y == 4) { //例外
						turnplayer_x++;
						turnplayer_y++;
						player[turnplayer]->set_x(turnplayer_x);
						player[turnplayer]->set_y(turnplayer_y);

						player[turnplayer]->set_direction(3);

						movepoint--;
					}
					else if (turnplayer_x == 6 && turnplayer_y == 4) { //例外
						turnplayer_x--;
						turnplayer_y++;
						player[turnplayer]->set_x(turnplayer_x);
						player[turnplayer]->set_y(turnplayer_y);

						player[turnplayer]->set_direction(2);

						movepoint--;
					}
					else if (map->get_MapEffects(turnplayer_y + 1, turnplayer_x) != 0 && turnplayer_y != 6) {
						player[turnplayer]->set_y(++turnplayer_y); //下へ
						turnplayer_y = player[turnplayer]->get_y();

						player[turnplayer]->set_direction(1);

						movepoint--;
					}

					PlaySoundMem(move_sound, DX_PLAYTYPE_BACK);

					Down = true;
				}
			}
			else {
				if (CheckHitKey(KEY_INPUT_DOWN) == 0 && input.Buttons[1] == 0) {
					Down = FALSE;
				}
			}
		}
		
	}
	else {
		//GMメッセージ
		draw_Text("GM", "ここでいい？\nyes:A ,no:B");

		//終わり
		if (Return == false) {
			if (GetKey(KEY_INPUT_RETURN) >= 1 || input.Buttons[12] >= 1) {
				for (int i = 0; i < 10; i++) {
					movedirection[i] = 0; //全ての方向の移動数を0にする
				}

				player[turnplayer]->set_direction(4);

				Return = true;

				flow = BATTLE;
			}
		}
		else {
			if (GetKey(KEY_INPUT_RETURN) == 0 && input.Buttons[12] == 0) {
				Return = false;
			}
		}
		
	}

	//キャンセル
	if (Q == false) {
		if (CheckHitKey(KEY_INPUT_Q) >= 1 || input.Buttons[13] >= 1) {

			move_cancel();

			Q = true;
		}
	}
	else {
		if (CheckHitKey(KEY_INPUT_Q) < 1 && input.Buttons[13] < 1) {
			Q = false;
		}
	}

	//battle関係
	engage();
}

void Gamemain::move_cancel() {

	PlaySoundMem(move_cancel_sound, DX_PLAYTYPE_BACK); //キャンセル音

	turnplayer_x = player[turnplayer]->get_Bx();
	turnplayer_y = player[turnplayer]->get_By();
	player[turnplayer]->set_x(turnplayer_x);
	player[turnplayer]->set_y(turnplayer_y); //座標リセット

	player[turnplayer]->set_direction(4); //初期値

	for (int i = 0; i < 10; i++) {
		movedirection[i] = 0; //全ての方向の移動数を0にする
	}

	movepoint = Bmovepoint; //移動数を戻す
	/*
	for (int i = 0; i < 5;i++){
		battle_Flag[i] = false;
		attacker[i] = 4; //初期値4(ドのプレイヤーも差さない)
		defender[i] = 4;
	}
	*/
	// 全戦闘削除
	attacker.clear();
	defender.clear();
	attacker.shrink_to_fit();
	defender.shrink_to_fit();

}

void Gamemain::engage() {
	for (int i = 0;i < 4;i++) {
		this->engageCFlag = false;
		if (turnplayer == i) {
			continue;
		}
		for (int j = 0; j < attacker.size(); j++) {	// 同じ相手と何度も戦わないように
			if (attacker[j] == turnplayer && defender[j] == i) {
				this->engageCFlag = true;
			}
		}
		if (this->engageCFlag) continue;
		if (turnplayer_x == player[i]->get_x() && turnplayer_y == player[i]->get_y()) { //接触したら
			if (player[i]->Queengetter() == true || player[turnplayer]->Queengetter() == true) {
				//battle_Flag[i] = true;
				//attacker[i] = turnplayer;
				//defender[i] = i;
				attacker.push_back(turnplayer);
				defender.push_back(i);
				break;
			}
		}
	}

}

void Gamemain::MapEffect() {
	switch (map->get_MapEffects(turnplayer_y, turnplayer_x)) {
	case 0://本当に何もなし
		draw_Text("エラー", "void Gamemain::MapEffect()でエラー");
		break;
	case 1:	// 効果なし
		flow = END;
		break;
	case 2:	// 城
		flow = END;
		break;
	case 3:	// １Pアジト
		if (turnplayer == 0 && player[turnplayer]->Queengetter() == true) { //嫁もち＋アジト
			winner = 0;
			nextMode = true;
		}
		flow = END;
		break;
	case 4:	// ２Pアジト
		if (turnplayer == 1 && player[turnplayer]->Queengetter() == true) { //嫁もち＋アジト
			winner = 1;
			nextMode = true;
		}
		flow = END;
		break;
	case 5:	// ３Pアジト
		if (turnplayer == 2 && player[turnplayer]->Queengetter() == true) { //嫁もち＋アジト
			winner = 2;
			nextMode = true;
		}
		flow = END;
		break;
	case 6:	// ４Pアジト
		if (turnplayer == 3 && player[turnplayer]->Queengetter() == true) { //嫁もち＋アジト
			winner = 3;
			nextMode = true;
		}
		flow = END;
		break;
	case 7:	// ワープマス

		if (wait == 64) {
			wait = 0;

			player[turnplayer]->set_x(4);
			player[turnplayer]->set_y(3); //ワープ

			if (turnplayer_x == 2) { //ワープの調整
				player[turnplayer]->set_inCastle(3);
			}
			else if (turnplayer_x == 4) {
				if (turnplayer_y == 1) {
					player[turnplayer]->set_inCastle(1);
				}
				else {
					player[turnplayer]->set_inCastle(0);
				}
			}
			else {
				player[turnplayer]->set_inCastle(2);
			}

			if (Hiace == false) { //初めての脱走
				player[turnplayer]->Queensetter(true);
				Hiace = true;
			}

			//ワープマスの設計ミスリペア
			//ここに書く
	
			flow = END;
		}
		else {
			draw_Text("システム", "ワープします");
			wait++;
		}
		
		break;
	case 8:	// メリットマス
		player[turnplayer]->dicenumsetter(2);
		flow = END;
		break;
	case 9:	// デメリットマス
		player[turnplayer]->dicecorrectionsetter(-1);
		flow = END;
		break;

	}
}

bool Gamemain::get_nextMode() {
	return nextMode;
}
int Gamemain::get_winner() {
	return winner;
}

int Gamemain::get_turnplayer() //ver1.1追加
{
	return turnplayer;
}