#pragma once
#include "DxLib.h"

// キーの入力状態を更新する
void Key_Update();

// 引数のキーコードのキーの入力状態を返す
int GetKey(int KeyCode);
