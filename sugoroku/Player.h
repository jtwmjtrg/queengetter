#pragma once
#include "Key.h"

typedef enum tag_PLAYER { RED, BLUE, YELLOW, GREEN }PLAYER; //キャラの色

class Player {
private:
	int x, y; //プレイヤーの座標
	int Bx, By; //直前の座標
	int Rx, Ry; //リス地点

	int direction; //向いている方向　上下左右0123
	int inCastle; //城に入った方向

	int dicenum; //振るダイスの数
	int dicecorrection; //補正値

	int PlayerXd;	// プレイヤーの位置のずれ（同じポイントにいるときに画像が重ならないように）
	int PlayerYd;

	bool QueenFlag; //姫所有判定

	int Gr_char; //キャラのグラフィック
	int Gr_charQ; //姫持ちグラフィック


public:
	Player(int No); //コンストラクタ、引数にプレイヤー番号を取る
	~Player();

	void death(); //死亡時判定
	void draw(int x, int y); //描画

	void set_x(int x);
	int get_x();
	void set_y(int y);
	int get_y();
	void set_Bx(int Bx);
	int get_Bx();
	void set_By(int By);
	int get_By();
	void set_direction(int direction);
	int get_direction(); //向きの雪駄ゲッター
	void set_inCastle(int Castle);
	int get_inCastle(); //城にいるかのセッタげった
	bool Queengetter(); //姫の所持判定のゲッタ
	void Queensetter(bool QueenFlag); //姫の所持判定のセッタ
	int dicenumgetter(); //ゲった
	void dicenumsetter(int dicenum); //振るダイスの数のセッタ
	int dicecorrectiongetter(); //補正値
	void dicecorrectionsetter(int dicecorrection); //ダイスの補正値のセッタ
};