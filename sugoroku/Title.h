#pragma once
#include "DxLib.h"
#include"Key.h"

class Title {
private:
	int Title_Graph; //タイトル画像
	int Title_BGM; //タイトルBGM
	int Title_Sound_SE; //決定音
	int carsol; //カーソル画像
	int y; //カーソルのy座標

	bool modeselect; //モード選択フラグ

	bool TitleBGMF; //タイトルサウンドのフラグ

	bool nextMode; //このフラグでフローが進行する
	bool end_Flag; //終了フラグ

	//ゲームパッド用あとで島田君が治す
	bool Return; //エンターキー対応
public:
	Title();
	~Title(); //コンデコ

	void drawTitle(); //描写
	void updateTitle(XINPUT_STATE input); //主に入力受付

	void set_nextMode(bool nextMode); //セッター
	bool get_nexctMode(); //ゲッター
	bool get_endFlag();
};