#pragma once
#include "Player.h"
#include "Map.h"
#include <vector>
#include <deque>

typedef enum tag_TurnFlow { START, DICEROLL, MOVE, BATTLE, EFFECTS, END }; //フロー管理

class Gamemain {
private:
	//音関係
	int backBGM;//後ろのBGM
	int BGMF;  //BGM流すフラグ
	int diceroll_sound; //ダイスのロール音
	int move_sound; //動くときの音
	int move_cancel_sound; //動きのキャンセル音
	int turn_end_sound; //ターンが変わるときの音

	bool gameend; //終了条件
	int Gr_dice[6]; //ダイスの画像
	int Gr_number[10]; //数字画像
	int Gr_TurnP[4];	// 誰のターンかの画像

	int turnplayer_x, turnplayer_y; //ターンプレイヤーの座標

	int text_Graph; //テキストボックスの画像
	int Color; //文字列の色


	tag_TurnFlow flow;
	int turnplayer; //今手番のプレイヤー
	int dicesum; //出目の合計
	int movepoint; //出目を受け取る
	int Bmovepoint; //直前の出目
	int wait; //出目表示時間
	bool diceroll_sound_Flag; //再生を一回だけするためのフラグ

	int movedirection[10]; //動いた方向を保存

	//姫関係
	int princess_Graph; //画像
	int princess_x, princess_y; //座標
	bool Hiace; //最初の脱走

	//battle関係
	int battle_Graph; //画像
	int attackpower;
	int defensepower;	//変数
	int battlecount;	// 何人に触れたか
	bool battle_Flag[5]; //battleするかどうか
	//int attacker[5]; //攻撃側(ターンプレイヤー)のid
	//int defender[5]; //防御側(姫持ち)のid
	std::deque<int>attacker;
	std::deque<int>defender;
	int BattleDelay;	// バトルシーンのディレイ
	int BattleStep;		// バトルシーン
	bool engageCFlag;
	bool dicestop;
	int attack_dice;
	int defence_dice;
	int rensen; //連戦処理
	int battle_x, battle_y; //描写関係の処理

	bool nextMode; //ゲーム進行管理
	int winner; //勝者

	//ゲームパッド
	bool Return, Left, Right, Up, Down;
	bool Q; //キャンセルのキー管理

	Player *player[4]; //4人
	Map *map; //マップ

	//ver1.1追加
	int backBGM2; //BGM２コ目
	bool BGMF2; //フラグも２コメ
public:
	Gamemain();
	~Gamemain();

	void update(XINPUT_STATE input);
	void draw();
	void move(XINPUT_STATE input);
	void move_cancel(); //何度も使うので関数化
	void engage(); //battle判定

	void MapEffect();	// マップ効果

	int diceroll(int dicenum, int dicemax, int correction); //汎用ダイスロール
	void draw_Text(char *name, char *s); //汎用テキスト
	void draw_Text(char *name, int x, char *s, int y); //overロード
	int battle(int attack, int defense); //戦闘処理(攻撃側、防御側)負けたプレイヤー番号を返す

	bool get_nextMode(); //ゲッター
	int get_winner(); //ゲッター

	int get_turnplayer(); //ver1.1追加
};

class Draw_Text {
private:
	bool Type;	// 汎用ならtrue
	char *name;	// 発言者
	char *s;	// 発現内容
	int x;		// %dの中身（name用）
	int y;		// %dの中身（s用）
	int wait;	// 表示フレーム
	int WHITE;	// 文字の色
public:
	Draw_Text(char *name, char *s, int wait); //汎用テキスト
	Draw_Text(char *name, int x, char *s, int y, int wait); //overロード
	~Draw_Text();

	bool UpDate();	// 消滅処理含む
	void Draw();
};