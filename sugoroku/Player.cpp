#include "Player.h"

Player::Player(int No) {//プレイヤーごとの初期情報
	
	//プレイヤーごとの初期情報
	//プレイヤー初期位置
	switch (No)
	{
	case RED:
		x = 0;
		y = 0;
		Gr_char = LoadGraph("素材\\OniRed.png"); //キャラの画像
		Gr_charQ = LoadGraph("素材\\OniHimeRed.png");
		PlayerXd = -32;	// プレイヤーの位置ずれ
		PlayerYd = -24;
		break;
	case BLUE:
		x = 8;
		y = 0;
		Gr_char = LoadGraph("素材\\OniBlue.png"); //キャラの画像
		Gr_charQ = LoadGraph("素材\\OniHimeBlue.png");
		PlayerXd = 32;
		PlayerYd = -24;
		break;
	case YELLOW:
		x = 0;
		y = 6;
		Gr_char = LoadGraph("素材\\OniYellow.png"); //キャラの画像
		Gr_charQ = LoadGraph("素材\\OniHimeYellow.png");
		PlayerXd = -32;
		PlayerYd = 24;
		break;
	case GREEN:
		x = 8;
		y = 6;
		Gr_char = LoadGraph("素材\\OniGreen.png"); //キャラの画像
		Gr_charQ = LoadGraph("素材\\OniHimeGreen.png");
		PlayerXd = 32;
		PlayerYd = 24;
		break;
	}

	direction = 4; //向いている方向　上下左右0123　4=初期化用
	inCastle = 4; //4=はいってない

	Bx = 0; 
	By = 0; //直前座標
	Rx = x;
	Ry = y; //リスポーン地点

	//ダイス関係の初期化
	dicenum = 1; //数
	dicecorrection = 0; //補正

	QueenFlag = false; //最初姫持っていないためfalse
}

Player::~Player() {

}

//死亡処理 プレイヤー位置をリスポーン地点に戻し、姫を破棄
void Player::death() {
	x = Rx;
	y = Ry;
	direction = 4;
	inCastle = 4;
	QueenFlag = false;
}

void Player::draw(int x, int y) {
	if (QueenFlag == true) {
		DrawRotaGraph(x + PlayerXd, y - 12 + PlayerYd, 0.8, 0, Gr_charQ, true, 0);
	}
	else {
		DrawRotaGraph(x + PlayerXd, y - 12 + PlayerYd, 0.8, 0, Gr_char, true, 0);
	}
	
}

void Player::set_x(int x) {
	this->x = x;
}
int Player::get_x() {
	return x;
}
void Player::set_y(int y) {
	this->y = y;
}
int Player::get_y() {
	return y;
}
void Player::set_Bx(int Bx) {
	this->Bx = Bx;
}
int Player::get_Bx() {
	return Bx;
}
void Player::set_By(int By) {
	this->By = By;
}
int Player::get_By() {
	return By;
}
void Player::set_direction(int direction) {
	this->direction = direction;
}
int Player::get_direction() {
	return direction;
}
void Player::set_inCastle(int inCastle) {
	this->inCastle = inCastle;
}
int Player::get_inCastle() {
	return inCastle;
}
bool Player::Queengetter() {
	return QueenFlag;
}
void Player::Queensetter(bool QueenFlag) {
	this->QueenFlag = QueenFlag;
}
int Player::dicenumgetter() {
	return dicenum;
}
void Player::dicenumsetter(int dicenum) {
	this->dicenum = dicenum;
}
int Player::dicecorrectiongetter() {
	return dicecorrection;
}
void Player::dicecorrectionsetter(int dicecorrection) {
	this->dicecorrection = dicecorrection;
}