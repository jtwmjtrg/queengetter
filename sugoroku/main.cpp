#include "Key.h"
#include "Map.h"
#include "Title.h"
#include "Gamemain.h"
#include "Result.h"

/*
2016/10/12ver1.1a実装
新規追加
・特になし
変更点
・ゲームパッド４個に対応
	タイトル画面とリザルト画面は１Pのみ対応（変更可能、仕様です）
	プレイ画面はターンプレイヤーのコントローラーが操作可能
	キーボードがすべての状態で操作可能な点は変更なし
・開発時のデバッグのため一部表記がキーボード表記だったものをゲームパッドの表記に変更
・バトル画面の自動進行を手動進行に変更
	手動で連射すると自動と同じ速度です
	具体的にはBattleDelayの遅延が延長される形です
・城のマスの色が変更されました
*/

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	//ウィンドウモード
	//ChangeWindowMode(TRUE);
	// 画面サイズ設定
	SetGraphMode(1920, 1080, 32);

	// ＤＸライブラリ初期化処理
	if (DxLib_Init() == -1)
	{
		return -1;        // エラーが起きたら直ちに終了
	}

	SetFontSize(64); //フォントサイズ

	SetDrawScreen(DX_SCREEN_BACK);

	bool end_Flag = false; //狩猟フラグ

	//進行管理
	typedef enum tag_Gamemode { TITLE, PLAY, RESULT };
	tag_Gamemode gamemode, Pre_gamemode;
	gamemode = TITLE;
	Pre_gamemode = gamemode; //直前のgamemode

	//ポインター 8/23少し変更
	Title *title = new Title();
	Gamemain *gamemain;
	Result *result;

	//ゲームパッド
	XINPUT_STATE input[4]; //ver1.1変更　input→input[4]

	int turnplayer = 0; //ver1.1追加 0:1P 3:4P

	int winner = 0; //勝ったプレイヤーのid

	while (ProcessMessage() != -1 && GetKey(KEY_INPUT_ESCAPE) == 0 && end_Flag == false && (input[0].Buttons[4] != 1 || input[0].Buttons[5] != 1)) {//画面更新 & メッセージ処理 & 画面消去
		ClearDrawScreen();

		GetJoypadXInputState(DX_INPUT_PAD1, &input[0]);
		//ver1.1追加/////////////////////////////////////////////////////
		GetJoypadXInputState(DX_INPUT_PAD2, &input[1]);
		GetJoypadXInputState(DX_INPUT_PAD3, &input[2]);
		GetJoypadXInputState(DX_INPUT_PAD4, &input[3]);
		////////////////////////////////////////////////////////////////

		// キー入力アップデート
		Key_Update();

		Pre_gamemode = gamemode; //直前のgamemode

		//進行管理
		switch (gamemode) {
		case TITLE:
			title->updateTitle(input[0]); //ver1.1変更　(input)→(input[0])
			title->drawTitle();
			if (title->get_nexctMode() == TRUE) { //フラグ受け取って次のモードへ
				DrawString(1500, 1000, "NowLoading…", GetColor(0, 0, 0));
				ScreenFlip();
				gamemode = PLAY;
			}

			if (title->get_endFlag() == true) {
				end_Flag = true;
			}
			break;
		case PLAY:
			//ver1.1追加 ターンプレイヤーの取得
			turnplayer = gamemain->get_turnplayer();
			gamemain->update(input[turnplayer]); //ver1.1変更　(input)→(input[turnplayer])
			gamemain->draw();
			//map->draw();
			if (gamemain->get_nextMode() == TRUE) { //フラグ受け取って次のモードへ
				gamemode = RESULT;
			}

			break;
		case RESULT:
			result->update_Result(input[0]); //ver1.1変更　(input)→(input[0])
			result->draw_Result();
			if (result->get_nexctMode() == TRUE) { //フラグを受け取って次のモードへ

				gamemode = TITLE;
			}
			break;
		default:
			//エラー
			break;
		}

		////////////////////////////////////////////////////////////////////////////////////// 8/23変更部分
		if (gamemode != Pre_gamemode) { //gamemode変更
			switch (Pre_gamemode) {
			case TITLE:
				delete title;
				gamemain = new Gamemain();
				turnplayer = 0; //ver1.1変更点
				break;
			case PLAY:
				winner = gamemain->get_winner();
				delete gamemain;
				result = new Result(winner);
				break;
			case RESULT:
				delete result;
				title = new Title();
				break;
			default:
				//エラー
				break;
			}
		}
		////////////////////////////////////////////////////////////////////////

		ScreenFlip();
	}

	// ＤＸライブラリ使用の終了処理
	DxLib_End();

	return 0;            // ソフトの終了
}

